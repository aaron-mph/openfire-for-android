package com.hzaccp.openfiretest;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smackx.filetransfer.FileTransfer;
import org.jivesoftware.smackx.filetransfer.FileTransfer.Status;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hzaccp.openfiretest.biz.DB;
import com.hzaccp.openfiretest.biz.SE;
import com.hzaccp.openfiretest.util.AndroidFileUtil;
import com.hzaccp.openfiretest.util.CallbackBundle;
import com.hzaccp.openfiretest.util.ExitUtil;

/**
 * 聊天界面
 * */
public class ChatActivity extends Activity {
	private String userName;//对方账号
	private String mPhotoPath;
	static private int openfileDialogId = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ExitUtil.getInstance().addActivity(this);//添加退出
		setContentView(R.layout.chat);
		init();
	}

	private void init() {
		SE.getInstance().setChatActivity(this);//当前聊天界面

		initEvent();

		//TODO 从临时会话里取信息
		String cu = SE.getInstance().getCu();
		String mess = DB.getInstance().getMess(cu);
		if (mess != null) {
			this.appMess(mess);
			DB.getInstance().clearMes(cu);//清空临时会话
		}
	}

	private void initEvent() {
		Button btnSend = (Button) findViewById(R.id.btnSend);
		Button btnCapture = (Button) findViewById(R.id.btnCapture);
		Button btnFile = (Button) findViewById(R.id.btnFile);

		final EditText txtChat = (EditText) findViewById(R.id.txtChat);

		//发送信息
		btnSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					String cu = SE.getInstance().getCu();
					XMPPConnection con = SE.getInstance().getCon();
					ChatManager cm = con.getChatManager();
					Chat chat = cm.createChat(cu, null);
					chat.sendMessage(txtChat.getText().toString());

					ChatActivity.this.appMess(txtChat.getText().toString());//添加信息
					txtChat.setText("");//清空信息框
				} catch (Exception err) {
					err.printStackTrace();
					Toast.makeText(ChatActivity.this, "发送信息出错", Toast.LENGTH_SHORT);
				}
			}
		});

		//截屏
		btnCapture.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
					mPhotoPath = Environment.getExternalStorageDirectory() + "/DCIM/Camera/" + Math.random() + ".png";
					File mPhotoFile = new File(mPhotoPath);
					if (!mPhotoFile.exists()) {
						mPhotoFile.createNewFile();
					}
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mPhotoFile));
					startActivityForResult(intent, 123);
				} catch (Exception e) {
				}
			}
		});

		//发送文件
		btnFile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showDialog(openfileDialogId);
			}
		});

	}

	/**
	 * 处理activity返回
	 * */
	@SuppressWarnings("unused")
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 123) {//123处理拍照相片返回
			if (mPhotoPath != null) {
				//得到图片大小但是不加载图片  
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				Bitmap bmp = BitmapFactory.decodeFile(mPhotoPath, options);
				int heightRatio = (int) Math.ceil(options.outHeight / (float) 480);
				int widthRatio = (int) Math.ceil(options.outWidth / (float) 320);
				if (heightRatio > 1 && widthRatio > 1) {
					if (heightRatio > widthRatio) {
						options.inSampleSize = heightRatio;
					} else {
						options.inSampleSize = widthRatio;
					}
				}
				options.inJustDecodeBounds = false;
				bmp = BitmapFactory.decodeFile(mPhotoPath, options);//写入
				sendFile(mPhotoPath, "PHOTO");
			}
		}
	}

	/**
	 * 发送文件
	 * */
	public void sendFile(final String fileName, final String type) {
		try {
			XMPPConnection con = SE.getInstance().getCon();
			FileTransferManager fileTransferManager = new FileTransferManager(con);
			String cu = SE.getInstance().getCu();
			final OutgoingFileTransfer fileTransfer = fileTransferManager.createOutgoingFileTransfer(cu + "/OPENFIRETEST");//OPENFIRETEST这登录时设置的参数
			File file = new File(fileName);
			fileTransfer.sendFile(file, type);//此处理type，在接收文件时，可以通过request.getDescription()取得此参数

			new Thread(new Runnable() {
				public void run() {
					while (true) {
						try {
							Thread.sleep(1000L);
							Status status = fileTransfer.getStatus();
							if ((status == FileTransfer.Status.error) || (status == FileTransfer.Status.complete) || (status == FileTransfer.Status.cancelled)
									|| (status == FileTransfer.Status.refused)) {
								android.os.Message message = handler.obtainMessage();
								if ("PHOTO".equals(type)) {//截屏
									message.what = 2;
								} else {//普通文件
									message.what = 1;
								}

								message.obj = fileName;
								message.sendToTarget();//发送成功
								break;//完成后退出
							}
						} catch (Exception err) {
						}
					}
				}
			}).start();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	/**
	 * 追加文本信息
	 * */
	public void appMess(String mess) {
		appMess(mess, 1);
	}

	/**
	 * 添加类型信息
	 * @param mess 信息
	 * @param type 类型 1=文本 2=图片 3=文件
	 * */
	public void appMess(String mess, int type) {
		LinearLayout ll = (LinearLayout) findViewById(R.id.linMess);
		switch (type) {
		case 1://文字内容
			TextView txtMess = new TextView(this);
			txtMess.setText(mess);
			ll.addView(txtMess);
			break;
		case 2://发送图片
			ImageView imgMess = new ImageView(this);
			imgMess.setAdjustViewBounds(true);
			imgMess.setMaxHeight(480);
			Bitmap bmp = BitmapFactory.decodeFile(mess);
			imgMess.setImageBitmap(bmp);
			ll.addView(imgMess);
			break;
		case 3://文件链接
			final TextView linMess = new TextView(this);
			linMess.setTextColor(Color.RED);
			linMess.setText(mess);
			linMess.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(AndroidFileUtil.openFile(linMess.getText().toString()));
				}
			});
			ll.addView(linMess);
			break;
		default:
			break;
		}
	}

	/**
	 * 打开文件选择框
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == openfileDialogId) {
			Map<String, Integer> images = new HashMap<String, Integer>();
			images.put(OpenFileDialog.sRoot, R.drawable.filedialog_root); // 根目录图标
			images.put(OpenFileDialog.sParent, R.drawable.filedialog_folder_up); //返回上一层的图标
			images.put(OpenFileDialog.sFolder, R.drawable.filedialog_folder); //文件夹图标
			images.put(OpenFileDialog.sEmpty, R.drawable.filedialog_root);
			Dialog dialog = OpenFileDialog.createDialog(id, this, "打开文件", new CallbackBundle() {
				@Override
				public void callback(Bundle bundle) {
					String filepath = bundle.getString("path");
					sendFile(filepath, "FILE");//发送文件
				}
			}, "", images);
			return dialog;
		}
		return null;
	}

	/**
	 * handler
	 * */
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1://普通文件传送成功
				appMess(msg.obj.toString() + "已发送", 3);
				break;
			case 2://截屏后文件传输成功
				appMess(msg.obj.toString(), 2);
				break;
			default:
				break;
			}
		};
	};

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 退出
	 * */
	@Override
	public void onBackPressed() {
		SE.getInstance().setCu(null);//清空当前聊天人
		SE.getInstance().setChatActivity(null);//清空当前聊天界面
		super.onBackPressed();
	}
}
