package com.hzaccp.openfiretest;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.hzaccp.openfiretest.biz.CO;
import com.hzaccp.openfiretest.biz.SE;
import com.hzaccp.openfiretest.util.ExitUtil;

/**
 * 好友列表界面
 * */
public class UsersActivity extends Activity {
	private ArrayList<HashMap<String, Object>> userList = null;//好友列表

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ExitUtil.getInstance().addActivity(this);//添加退出
		setContentView(R.layout.users);
		init();
	}

	private void init() {
		SE.getInstance().setUserActivity(this);//设置当前活动界面为UserActivity

		//添加监听
		CO.getInstance().messListener();//普通信息
		CO.getInstance().fileListener();//文件信息

		//初始化用户列表
		CO.getInstance().getUserList();

		//事件监听
		initEvent();

		Toast.makeText(this, "登录成功", Toast.LENGTH_LONG).show();
	}

	private void initEvent() {
		//列表的监听事件
		ListView list = (ListView) findViewById(R.id.userListView);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> aView, View view, int index, long arg3) {
				//打开聊天界面
				SimpleAdapter listItemAdapter = (SimpleAdapter) aView.getAdapter();

				@SuppressWarnings("unchecked")
				HashMap<String, Object> hmap = (HashMap<String, Object>) listItemAdapter.getItem(index);
				SE.getInstance().setCu(hmap.get("userId").toString());

				updateNewMess(hmap.get("userId").toString(), 0);//清空未读信息

				Intent intent = new Intent(UsersActivity.this, ChatActivity.class);
				startActivity(intent);
			}
		});
	}

	/**
	 * 在行处添加有未读信息标记
	 * */
	public void updateNewMess(String form, int c) {
		if (form == null) {
			return;
		}
		ListView list = (ListView) findViewById(R.id.userListView);
		SimpleAdapter sap = (SimpleAdapter) list.getAdapter();

		//下面算法没有优化，应重写业务
		try {
			for (int i = 0; i < userList.size(); i++) {
				HashMap<String, Object> m = userList.get(i);
				if (form.equals(m.get("userId").toString())) {
					String oldStr = m.get("userName").toString();
					String newStr = "";
					if (c == 0) {
						newStr = oldStr.split("（")[0];
					} else if (oldStr.indexOf("（") > -1 && oldStr.indexOf("）") > -1) {//已经有数据
						newStr = oldStr.split("（")[0];
						int count = Integer.parseInt(oldStr.split("（")[1].split("）")[0]) + 1;
						newStr += "（" + count + "）";
					} else {//无数据
						newStr = m.get("userName").toString() + "（1）";
					}
					m.remove("userName");
					m.put("userName", newStr);
					break;
				}
			}
		} catch (Exception err) {
			err.printStackTrace();
		}
		sap.notifyDataSetChanged();
	}

	/**
	 * 初始化我的好友
	 * */
	public void initUserList(ArrayList<HashMap<String, Object>> listItem) {
		userList = listItem;
		ListView list = (ListView) findViewById(R.id.userListView);
		SimpleAdapter listItemAdapter = new SimpleAdapter(this, listItem, R.layout.listrow, new String[] { "userName", "userId" }, new int[] { R.id.userName,
				R.id.userId });
		list.setAdapter(listItemAdapter);
	}

	/**
	 * 退出
	 * */
	@Override
	public void onBackPressed() {
		ExitUtil.getInstance().exit(this);
	}
}
